package empikfuturestars.demo.service;

import org.springframework.stereotype.Service;

@Service
public class AdditionService {

    public int add(int[] numbers) {
        int result = 0;
        for (int num : numbers) {
            result = result + num;
        }
        if (result > 1000) {
            result = numbers[0];
        }
        return result;
    }
}
