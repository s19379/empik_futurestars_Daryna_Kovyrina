package empikfuturestars.demo.controller;

import empikfuturestars.demo.service.AdditionService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.Arrays;

@RestController
@RequestMapping(path="api")
public class AdditionController {

    private final AdditionService additionService;

    @Autowired
    public AdditionController(AdditionService additionService) {
        this.additionService = additionService;
    }

    @Operation(summary = "Get Sum of Numbers")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Request finished with success"),
            @ApiResponse(responseCode = "404", description = "Not found"),
            @ApiResponse(responseCode = "500", description = "Bad Request. More info in logs.")})
    @GetMapping("/add")
    @ResponseBody
    public int add(@RequestParam String numbers) {
        if (numbers.isEmpty()) {
            return 0;
        }

        numbers = numbers.replaceAll("[^-0-9.]+", ",");
        numbers = numbers.startsWith(",") ? numbers.substring(1) : numbers;

        int[] ints = Arrays.stream(numbers.replaceAll("-", " -").split("[^-\\d]+"))
                .filter(s -> !s.matches("-?"))
                .mapToInt(Integer::parseInt).toArray();

            for(int i = 0; i < ints.length; i++) {
                if (ints[i] < 0) {
                    throw new IllegalArgumentException("Negatives are not allowed.");
                }
            }
        return additionService.add(ints);
    }

}
