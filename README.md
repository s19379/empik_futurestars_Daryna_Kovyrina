# empik_futurestars_Daryna_Kovyrina

Sample requests:

###
GET http://localhost:8080/api/add?numbers=1\n2,3

###
GET http://localhost:8080/api/add?numbers=//;\n1;2

###
GET http://localhost:8080/api/add?numbers=//[*][,]\n9*-10,4

###
GET http://localhost:8080/api/add?numbers=//[***]\n1***2***3

###
GET http://localhost:8080/api/add?numbers=2\n1001

###
GET http://localhost:8080/api/add?numbers=//[*][$]\n1*2$3

